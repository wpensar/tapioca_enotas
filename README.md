# Tapioca enotas

## Installation
```
pip install tapioca-enotas
```

## Documentation
``` python
from tapioca_enotas import Enotas


api = Enotas(token='{your-token}')

```

No more documentation needed.

- Learn how Tapioca works [here](http://tapioca-wrapper.readthedocs.org/en/stable/quickstart.html)
- Explore this package using iPython
- Have fun!
